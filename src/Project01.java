/**
 * Created by Christian Cataldo on 08/11/2016.
 */

package it.ccataldo345.project2016;

import lib.TextIO;

public class Project01 {
    public static void main(String[] args) {
        System.out.println("Project by Christian Cataldo, IT College Tallinn, Cyber Security Engineering, 2016");
        System.out.println(name() + " has " + money() + " Euros.");
        System.out.println(name() + " will earn " + gain() + " Euros in " + period() + " years!");
    }


    public static String name() {
        System.out.println("Please enter your name: ");
        String name01 = TextIO.getlnString();
        return name01;
    }

    public static double money() {
        System.out.println("Please enter amount in Euros: ");
        double money01 = TextIO.getlnDouble();
        return money01;
    }

    public static double period() {
        System.out.println("Please enter time lenght of deposit in years: ");
        double time01 = TextIO.getlnDouble();
        return time01;
    }

    public static double gain() {
        double yield01 = 0.035;
        double gain01 = money() * period() * yield01;
        return gain01;
    }
}
